import { ThemeOptions, createTheme } from "@mui/material";

const defaultOptions: ThemeOptions = {
  components: {
    MuiAutocomplete: {
      styleOverrides: {
        root: {
          "& .MuiInputBase-root": {
            ".MuiInputBase-input": {
              padding: "0px !important",
            },
            height: 43,
          },
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: 50,
          textTransform: "none",
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          "& .MuiOutlinedInput-root": {
            padding: 0,
            "& .MuiAutocomplete-input": {
              padding: 10,
            },
          },
        },
        input: {
          padding: 10,
        },
      },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          fontSize: "1rem",
        },
      },
    },
  },
  typography: {
    h5: {
      fontWeight: "bold"
    }
  }
};

export const lightTheme = createTheme({
  palette: {
    primary: {
      main: "#118D43",
      light: "#92E7DD",
      dark: "#019367",
    },
    text: {
      primary: "#000000",
    },
    background: {
      default: "#F7F2F2",
    },
    mode: "light",
  },
  ...defaultOptions,
});

export const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#72EEA4",
      dark: "#186D63",
      light: "#6CFED2",
    },
    text: {
      primary: "#EAEAEA",
    },
    background: {
      default: "#120E0E",
    },
    mode: "dark",
  },
  ...defaultOptions,
});
