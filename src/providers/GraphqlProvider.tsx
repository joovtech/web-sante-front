import React from "react";
import { ApolloProvider } from "@apollo/client";
import { AuthStorage } from "../utils/AuthStorage";
import { apolloClient } from "../apolloClient";

type GraphqlProviderProps = {
  children: React.ReactNode;
};

export const GraphqlProvider = ({ children }: GraphqlProviderProps) => {
  const tokenFromstorage = AuthStorage.isAuth()?.token;
  const client = apolloClient(tokenFromstorage);

  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};
