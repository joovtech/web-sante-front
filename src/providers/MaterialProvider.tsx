import { CssBaseline, ThemeProvider } from "@mui/material";
import { StylesProvider } from "@mui/styles";
import React from "react";
import { useApplicationHook } from "../hook/application/useApplicationHook";

type MaterialProviderProps = {
  children: React.ReactNode;
};

export const MaterialProvider = ({
  children,
}: MaterialProviderProps): JSX.Element => {
  const { theme } = useApplicationHook();
  return (
    <ThemeProvider theme={theme}>
      <StylesProvider injectFirst>
        <CssBaseline />
        {children}
      </StylesProvider>
    </ThemeProvider>
  );
};
