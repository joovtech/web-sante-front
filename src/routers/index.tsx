import { Route, Routes } from "react-router-dom";
import { PrivateRoute } from "./PrivateRoute";
import React, { Suspense } from "react";
import { CircularProgress } from "@mui/material";
import { ContainerWithMenu } from "../components/layout/ContainerWithMenu";
const Chat = React.lazy(() => import("../pages/Chat"));

const AuthenticationRoute = React.lazy(
  () => import("./public/AuthenticationRoute")
);

const PrivateRouter = (): JSX.Element => {
  return (
    <Suspense fallback={<CircularProgress />}>
      <Routes>
        <Route index element={<Chat />} />
        <Route path="discussion/:id" element={<Chat />} />
      </Routes>
    </Suspense>
  );
};

export const MainRouter = (): JSX.Element => {
  return (
    <Routes>
      <Route
        path="/auth/*"
        element={
          <Suspense fallback={<CircularProgress />}>
            <AuthenticationRoute />
          </Suspense>
        }
      />
      <Route
        path="/*"
        element={
          <PrivateRoute>
            <ContainerWithMenu>
              <PrivateRouter />
            </ContainerWithMenu>
          </PrivateRoute>
        }
      />
    </Routes>
  );
};
