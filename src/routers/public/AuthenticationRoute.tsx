import { Navigate, Route, Routes } from "react-router-dom";
import { useApplicationHook } from "../../hook/application/useApplicationHook";
import Login from "../../pages/auth/Login";
import Signup from "../../pages/auth/Signup";

const AuthenticationRoute = () => {
  const { user } = useApplicationHook();

  if (user) {
    return <Navigate to="/" />;
  }

  return (
    <Routes>
      <Route path="login" element={<Login />} />
      <Route path="signup" element={<Signup />} />
    </Routes>
  );
};

export default AuthenticationRoute;
