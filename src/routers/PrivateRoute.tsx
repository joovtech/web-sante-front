import { Navigate } from "react-router-dom";
import { useApplicationHook } from "../hook/application/useApplicationHook";

export const PrivateRoute = ({
  children,
}: {
  children: JSX.Element;
}): JSX.Element => {
  const { user } = useApplicationHook();

  if (user) {
    return children;
  }
  return <Navigate to="/auth/login" />;
};
