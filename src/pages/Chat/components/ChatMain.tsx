import { Box, Typography, styled } from "@mui/material";
import { useDiscussion } from "../../../store/useDiscussion";

const suggestions: string[] = [
  "Comment soigner rapidement la diarhée",
  "Comment soigner rapidement la diarhée",
  "Comment soigner rapidement la diarhée",
];

const ItemBox = styled(Box)(({ theme }) => ({
  padding: "8px",
  borderRadius: "10px",
  cursor: "pointer",
  background:
    theme.palette.mode === "light"
      ? theme.palette.primary.light
      : theme.palette.primary.dark,
}));

export const ChatMain = () => {
  const { curr } = useDiscussion();
  return (
    <Box sx={{ px: 1 }}>
      {!curr ? (
        <>
          <Typography variant="h6">
            Vous pouvez essayer de me demander:
          </Typography>

          <Box
            sx={{
              display: "flex",
              justifyContent: "space-around",
              flexFlow: "row wrap",
              gap: 2,
              my: 2,
            }}
          >
            {suggestions.map((i, index) => (
              <ItemBox key={index}>{i}</ItemBox>
            ))}
          </Box>
        </>
      ) : (
        <Box></Box>
      )}
    </Box>
  );
};
