import { Box, Typography } from "@mui/material";
import logo from "../../../assets/logo.png";
import { useDiscussion } from "../../../store/useDiscussion";
import { useMemo } from "react";

export const ChatHead = () => {
  const { curr } = useDiscussion();

  const title = useMemo(
    () =>
      curr
        ? curr.title
        : "Rejoignez le futur de la santé avec notre assistance médicale innovante, alimentée par l'intelligence artificielle. Nous nous engageons à créer une expérience personnalisée et inclusive, où chaque patient est vu et traité avec soin et attention. Ensemble, explorons les possibilités infinies offertes par l'IA pour améliorer votre bien-être.",
    [curr]
  );
  return (
    <Box sx={{ px: 1 }}>
      <Box sx={{ display: "flex", justifyContent: "center", my: 2 }}>
        <Box component="img" src={logo} sx={{ width: "60px" }} />
      </Box>
      <Typography variant="h5" sx={{ mb: 1 }}>
        Assistance médicale AI
      </Typography>
      <Typography sx={{ mb: 1, textAlign: "justify" }}>{title}</Typography>
    </Box>
  );
};
