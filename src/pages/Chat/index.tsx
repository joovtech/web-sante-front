import { Box, IconButton, TextField, useTheme } from "@mui/material";
import { ChatHead } from "./components/ChatHead";
import { ChatMain } from "./components/ChatMain";
import AddIcon from "@mui/icons-material/Add";
import SendIcon from "../../assets/send.svg";

const Chat = () => {
  const theme = useTheme();
  return (
    <Box
      sx={{
        borderRadius: "20px",
        background: theme.palette.mode === "light" ? "#F3F9F5" : undefined,
        width: "100%",
        height: "89vh",
        position: "relative",
        overflowY: "hidden",
        p: 2,
      }}
    >
      <Box sx={{ height: "95%", overflowY: "auto" }}>
        <ChatHead />
        <ChatMain />
      </Box>
      <Box
        sx={{
          position: "absolute",
          bottom: 0,
          width: { xs: "92%", md: "96%" },
        }}
      >
        <TextField
          fullWidth
          InputProps={{
            startAdornment: (
              <IconButton>
                <AddIcon color="primary" />
              </IconButton>
            ),
            endAdornment: (
              <IconButton
                sx={{
                  borderRadius: "1px",
                  background: (theme) =>
                    theme.palette.mode === "light"
                      ? theme.palette.primary.light
                      : theme.palette.primary.dark,
                }}
              >
                <Box component="img" src={SendIcon} sx={{ width: "15px" }} />
              </IconButton>
            ),
          }}
        />
      </Box>
    </Box>
  );
};

export default Chat;
