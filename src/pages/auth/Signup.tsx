import {
  Box,
  Button,
  Card,
  Container,
  TextField,
  useTheme,
} from "@mui/material";
import { useState } from "react";
import { useApplicationHook } from "../../hook/application/useApplicationHook";
import { SignupInput } from "../../gql/graphql";
import logo from "../../assets/logo.png";
import { useForm } from "react-hook-form";
import EmailIcon from "@mui/icons-material/Email";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import { NavLink } from "react-router-dom";

const Signup = () => {
  const [showPass, setShowPass] = useState<boolean>(false);
  const { signupApp, signupLoading } = useApplicationHook();
  const {
    formState: { errors },
    register,
    handleSubmit,
  } = useForm<SignupInput>();
  const theme = useTheme();
  const onConnect = async (data: SignupInput) => {
    console.log("makato alou");
    await signupApp(data);
  };
  return (
    <Container
      sx={{
        minHeight: "100vh",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Card
        elevation={1}
        sx={{ width: { xs: "100%", md: "50%" }, p: 2, borderRadius: "30px" }}
      >
        <Box sx={{ display: "flex", justifyContent: "center", mb: 2 }}>
          <Box component="img" src={logo} sx={{ width: "70px" }} />
        </Box>
        <Box
          component="form"
          // onSubmit={handleSubmit(onConnect)}
          sx={{ px: 1, my: 1 }}
        >
          <TextField
            fullWidth
            sx={{ my: 1 }}
            placeholder="User name"
            {...register("username")}
          />
          <TextField
            fullWidth
            error={errors.email && true}
            sx={{ my: 1 }}
            type="email"
            placeholder="Email"
            {...register("email", {
              required: {
                value: true,
                message: "Vous devez entrer votre email",
              },
              pattern: {
                value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                message: "entrer un email valide",
              },
            })}
            InputProps={{ endAdornment: <EmailIcon /> }}
            helperText={errors.email?.message}
          />
          <TextField
            fullWidth
            error={errors.password && true}
            placeholder="Password"
            sx={{ my: 1 }}
            type={showPass ? "text" : "password"}
            {...register("password", {
              required: {
                value: true,
                message: "votre mot de passe est requis",
              },
            })}
            InputProps={{
              endAdornment: showPass ? (
                <VisibilityOffIcon
                  sx={{ cursor: "pointer" }}
                  onClick={() => setShowPass((curr) => !curr)}
                />
              ) : (
                <VisibilityIcon
                  sx={{ cursor: "pointer" }}
                  onClick={() => setShowPass((curr) => !curr)}
                />
              ),
            }}
            helperText={errors.password?.message}
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            px: 1,
            my: 1,
          }}
        >
          <NavLink
            to="/auth/login"
            style={{
              color: theme.palette.primary.main,
              fontSize: "0.7em",
              textDecoration: "none",
            }}
          >
            Vous avez déjà compte?
          </NavLink>
          <NavLink
            to="#"
            style={{
              color: theme.palette.text.primary,
              textDecoration: "none",
              fontSize: "0.7em",
            }}
          >
            Mot de passe oublié
          </NavLink>
        </Box>
        <Box sx={{ display: "flex", justifyContent: "center", my: 1 }}>
          <Button
            variant="outlined"
            disabled={signupLoading}
            onClick={handleSubmit(onConnect)}
          >
            S'inscrire
          </Button>
        </Box>
      </Card>
    </Container>
  );
};

export default Signup;
