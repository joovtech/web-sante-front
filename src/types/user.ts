import { GetUserQuery, LoginMutation, Scalars } from "../gql/graphql";

export type LoginData = LoginMutation["login"]["data"];
export type GetUserData = GetUserQuery["getUser"];
export type CurrentDiscussion = {
  __typename?: "Discussion";
  id: Scalars["Int"]["output"];
  title: Scalars["String"]["output"];
} | null | undefined;
