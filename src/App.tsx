import "./App.css";
import { BrowserRouter } from "react-router-dom";
import { MainRouter } from "./routers";
import { GraphqlProvider } from "./providers/GraphqlProvider";
import { MaterialProvider } from "./providers/MaterialProvider";

function App() {
  return (
    <BrowserRouter>
      <GraphqlProvider>
        <MaterialProvider>
          <MainRouter />
        </MaterialProvider>
      </GraphqlProvider>
    </BrowserRouter>
  );
}

export default App;
