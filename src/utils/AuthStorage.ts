import { LOCALSTORAGE } from "../constants";
import { LoginData } from "../types/user";

export const AuthStorage = {
  isAuth: (): LoginData | undefined => {
    if (typeof window === undefined) return undefined;
    if (localStorage.getItem(LOCALSTORAGE))
      return JSON.parse(localStorage.getItem(LOCALSTORAGE) as string) as LoginData;
    return undefined;
  },
  authenticate: (user: LoginData, callback?: (data: LoginData) => void): unknown => {
    if (typeof window !== undefined) {
      localStorage.setItem(LOCALSTORAGE, JSON.stringify(user));
      return callback && callback(user);
    }
    return undefined;
  },
  clearToken: (callback: () => void) => {
    if (typeof window !== undefined) localStorage.clear();
    callback();
  },
};

export const LocalStorage = {
  isAuth: (): LoginData | undefined => {
    if (typeof window === undefined) return undefined;
    if (localStorage.getItem(LOCALSTORAGE))
      return JSON.parse(localStorage.getItem(LOCALSTORAGE) as string) as LoginData;
    return undefined;
  },
  authenticate: (
    business: LoginData,
    callback?: (data: LoginData) => void
  ): unknown => {
    if (typeof window !== undefined) {
      localStorage.setItem(LOCALSTORAGE, JSON.stringify(business));
      return callback && callback(business);
    }
    return undefined;
  },
  clearToken: (callback: () => void) => {
    localStorage.clear();
    callback();
  },
};
