import {
  Box,
  Button,
  FormControlLabel,
  FormGroup,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Switch,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useApplicationHook } from "../../hook/application/useApplicationHook";
import { useState } from "react";
import { darkTheme, lightTheme } from "../../theme";
import { useMutation, useQuery } from "@apollo/client";
import {
  GetDiscussionsUserQuery,
  GetDiscussionsUserQueryVariables,
  UpdateUserMutation,
  UpdateUserMutationVariables,
  UserMode,
} from "../../gql/graphql";
import { UPDATE_INFO } from "../../graphql/user";
import { GET_DISCUSSIONS } from "../../graphql/discussion";
import { useNavigate, useParams } from "react-router-dom";
import { useDiscussion } from "../../store/useDiscussion";

const MaterialUISwitch = styled(Switch)(({ theme }) => ({
  width: 62,
  height: 34,
  padding: 7,
  "& .MuiSwitch-switchBase": {
    margin: 1,
    padding: 0,
    transform: "translateX(6px)",
    "&.Mui-checked": {
      color: "#fff",
      transform: "translateX(22px)",
      "& .MuiSwitch-thumb:before": {
        backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 20 20"><path fill="${encodeURIComponent(
          "#fff"
        )}" d="M4.2 2.5l-.7 1.8-1.8.7 1.8.7.7 1.8.6-1.8L6.7 5l-1.9-.7-.6-1.8zm15 8.3a6.7 6.7 0 11-6.6-6.6 5.8 5.8 0 006.6 6.6z"/></svg>')`,
      },
      "& + .MuiSwitch-track": {
        opacity: 1,
        backgroundColor: theme.palette.mode === "dark" ? "#92E7DD" : "#92E7DD",
      },
    },
  },
  "& .MuiSwitch-thumb": {
    backgroundColor: theme.palette.mode === "dark" ? "#72EEA4" : "#118D43",
    width: 32,
    height: 32,
    "&::before": {
      content: "''",
      position: "absolute",
      width: "100%",
      height: "100%",
      left: 0,
      top: 0,
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 20 20"><path fill="${encodeURIComponent(
        "#fff"
      )}" d="M9.305 1.667V3.75h1.389V1.667h-1.39zm-4.707 1.95l-.982.982L5.09 6.072l.982-.982-1.473-1.473zm10.802 0L13.927 5.09l.982.982 1.473-1.473-.982-.982zM10 5.139a4.872 4.872 0 00-4.862 4.86A4.872 4.872 0 0010 14.862 4.872 4.872 0 0014.86 10 4.872 4.872 0 0010 5.139zm0 1.389A3.462 3.462 0 0113.471 10a3.462 3.462 0 01-3.473 3.472A3.462 3.462 0 016.527 10 3.462 3.462 0 0110 6.528zM1.665 9.305v1.39h2.083v-1.39H1.666zm14.583 0v1.39h2.084v-1.39h-2.084zM5.09 13.928L3.616 15.4l.982.982 1.473-1.473-.982-.982zm9.82 0l-.982.982 1.473 1.473.982-.982-1.473-1.473zM9.305 16.25v2.083h1.389V16.25h-1.39z"/></svg>')`,
    },
  },
  "& .MuiSwitch-track": {
    opacity: 1,
    backgroundColor: theme.palette.mode === "dark" ? "#92E7DD" : "#92E7DD",
    borderRadius: 20 / 2,
  },
}));

const DiscussionItem = styled(ListItem)(({ theme }) => ({
  borderRadius: "10px",
  background: theme.palette.mode === "dark" ? "#2C322E" : "#F3F9F5",
  padding: "0px",
}));

export const DrawerContent = () => {
  const { changeTheme, theme, user } = useApplicationHook();
  const [checked, setChecked] = useState<boolean>(
    theme.palette.mode === "dark"
  );
  const { setCurrent } = useDiscussion();

  const navigate = useNavigate();
  const { id } = useParams();

  const { data: discussions, loading: discussionLoading } = useQuery<
    GetDiscussionsUserQuery,
    GetDiscussionsUserQueryVariables
  >(GET_DISCUSSIONS, {
    variables: { userId: user?.id as number, cursor: null },
    skip: !user?.id,
    notifyOnNetworkStatusChange: true,
  });

  const [exec] = useMutation<UpdateUserMutation, UpdateUserMutationVariables>(
    UPDATE_INFO
  );

  const handleChange = async (_event: React.ChangeEvent<HTMLInputElement>) => {
    if (!user) return;
    await exec({
      variables: {
        updateUserInput: { mode: checked ? UserMode.Dark : UserMode.Light },
        userId: user.id,
      },
    });
    setChecked((curr) => !curr);
    changeTheme(checked ? lightTheme : darkTheme);
  };
  return (
    <Box sx={{ p: 2, position: "relative", height: "100%", overflowY: "auto" }}>
      <Typography>Nom du Projet</Typography>
      <Box sx={{ my: 2 }}>
        <Button variant="outlined" fullWidth>
          <AddIcon />
          New Chat
        </Button>
        <TextField fullWidth sx={{ mt: 1 }} placeholder="Rechercher" />
      </Box>

      <List>
        {discussions?.getDiscussionsUser.map((disc) => (
          <DiscussionItem key={disc.id}>
            <ListItemButton
              onClick={() => {
                setCurrent(disc);
                navigate(`/discussion/${disc.id}`);
              }}
            >
              <ListItemText>{disc.title}</ListItemText>
            </ListItemButton>
          </DiscussionItem>
        ))}
      </List>

      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          position: "absolute",
          width: "90%",
          bottom: 10,
        }}
      >
        <FormGroup>
          <FormControlLabel
            control={
              <MaterialUISwitch
                onChange={handleChange}
                sx={{ m: 1 }}
                checked={checked}
              />
            }
            label=""
          />
        </FormGroup>
      </Box>
    </Box>
  );
};
