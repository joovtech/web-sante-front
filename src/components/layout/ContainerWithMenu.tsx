import {
  Avatar,
  Box,
  BoxProps,
  CssBaseline,
  Divider,
  Drawer,
  IconButton,
  styled,
  useTheme,
} from "@mui/material";
import { FC, useState } from "react";
import { DrawerContent } from "./DrawerContent";
import defaultProfil from "../../assets/defaultProfil.png";
import SettingsIcon from "@mui/icons-material/Settings";

const DashboardLayoutRoot = styled("div")(() => ({
  display: "flex",
  flex: "1 1 auto",
  background: "#006949",
}));

type ContainerWithDrawerProps = {
  open: boolean;
  onClose: () => void;
} & BoxProps;
const drawerWidth = 260;
const ContainerWithDrawer: FC<ContainerWithDrawerProps> = ({
  open,
  onClose,
  children,
}) => {
  const theme = useTheme();
  return (
    <Box
      sx={{
        display: "flex",
        width: { xs: "100%", md: "89%" },
        height: "100%",
        justifyContent: "space-around",
        background: theme.palette.background.default,
        borderRadius: { md: "20px" },
      }}
    >
      <CssBaseline />
      <Drawer
        variant="temporary"
        open={open}
        onClose={onClose}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: "block", md: "none" },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: drawerWidth,
          },
        }}
      >
        <DrawerContent />
      </Drawer>
      <Box
        component="nav"
        sx={{
          width: { sm: drawerWidth },
          flexShrink: { sm: 0 },
          display: { xs: "none", md: "block" },
          py: { md: 2 },
        }}
      >
        <Drawer
          variant="permanent"
          PaperProps={{
            sx: { position: "sticky", top: 0, left: 0, borderRadius: "20px" },
            elevation: 1,
          }}
          sx={{
            display: { xs: "none", md: "block" },
            position: "relative",
            height: "100%",
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
          open
        >
          <DrawerContent />
        </Drawer>
      </Box>
      <Box
        component="main"
        sx={{
          py: { md: 2 },
          width: { md: `calc(100% - ${330}px)` },
        }}
      >
        {children}
      </Box>
    </Box>
  );
};

export const ContainerWithMenu = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [openMobileDrawer, setOpenMobileDrawer] = useState(false);
  return (
    <DashboardLayoutRoot>
      <Box
        sx={{
          display: "flex",
          width: "100%",
          minHeight: "100vh",
          overflowY: "hidden",
          flexFlow: "row wrap",
          py: { md: 2 },
        }}
      >
        <Box
          sx={{
            width: { xs: "100%", md: "10%" },
            height: { md: "100%" },
            display: "flex",
            flexDirection: { xs: "row", md: "column" },
            alignItems: "center",
            position: { xs: "sticky", md: "initial" },
            top: 0,
            background: "#006949",
            justifyContent: "space-between",
            p: { xs: 1, md: undefined },
          }}
        >
          <IconButton
            onClick={() => setOpenMobileDrawer(true)}
            sx={{ display: { xs: "block", md: "none" } }}
          >
            <SettingsIcon />
          </IconButton>
          <Box>
            <Avatar src={defaultProfil} sx={{ width: 48, height: 48 }} />
            <Divider color="white" variant="fullWidth" sx={{ mt: 1, display: { xs: "none", md: "block" } }} />
          </Box>
        </Box>
        <ContainerWithDrawer
          open={openMobileDrawer}
          onClose={() => setOpenMobileDrawer(false)}
        >
          {children}
        </ContainerWithDrawer>
      </Box>
    </DashboardLayoutRoot>
  );
};
