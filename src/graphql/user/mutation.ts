import { gql } from "@apollo/client";

export const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      message
      success
      data {
        id
        token
        email
        username
        mode
        status
        photo
        createdAt
        updatedAt
      }
    }
  }
`;

export const SIGNUP = gql`
  mutation signup($userInput: SignupInput!) {
    signup(userInput: $userInput) {
      message
      success
      data {
        id
        email
        username
        status
        mode
        photo
        createdAt
        updatedAt
        token
      }
    }
  }
`;

export const UPDATE_INFO = gql`
  mutation UpdateUser($updateUserInput: UpdateUserInput!, $userId: Float!) {
    updateUser(updateUserInput: $updateUserInput, userId: $userId)
  }
`;
