import { gql } from "@apollo/client";

export const GET_USER = gql`
  query GetUser($userId: Float!) {
    getUser(userId: $userId) {
      id
      email
      username
      mode
      status
      photo
      createdAt
      updatedAt
    }
  }
`;
