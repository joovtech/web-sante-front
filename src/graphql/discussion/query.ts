import { gql } from "@apollo/client";

export const GET_DISCUSSIONS = gql`
  query GetDiscussionsUser($userId: Float!, $cursor: Float) {
    getDiscussionsUser(userId: $userId, cursor: $cursor) {
      id
      title
    }
  }
`;

export const GET_MESSAGES = gql`
  query GetMessagesOfDiscussion($discussionId: Float!, $cursor: Float) {
    getMessagesOfDiscussion(discussionId: $discussionId, cursor: $cursor) {
      id
      content
      updatedAt
      createdAt
      files {
        id
        name
        extension
        url
      }
    }
  }
`;
