import { gql } from "@apollo/client";

export const SEND_MESSAGE = gql`
  mutation SendMessage($messageInput: MessageInput!) {
    sendMessage(messageInput: $messageInput) {
      id
      title
    }
  }
`;
