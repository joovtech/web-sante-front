import { create } from "zustand";
import { CurrentDiscussion } from "../types/user";

type IDiscussion = {
  curr: CurrentDiscussion;
  setCurrent: (val: CurrentDiscussion) => void;
};

export const useDiscussion = create<IDiscussion>()((set) => ({
  curr: null,
  setCurrent: (val) => set((state) => ({ ...state, curr: val })),
}));
