import { create } from "zustand";
import { LoginData } from "../types/user";
import { Theme } from "@mui/material";
import { darkTheme, lightTheme } from "../theme";
import { UserMode } from "../gql/graphql";

type IApplication = {
  user: LoginData;
  theme: Theme;
  login: (val: LoginData) => void;
  changeTheme: (val: Theme) => void;
  logout: () => void;
};
export const useApplication = create<IApplication>()((set) => ({
  user: null,
  theme: lightTheme,
  login: (val) =>
    set((state) => ({
      ...state,
      user: val,
      theme: val?.mode === UserMode.Dark ? darkTheme : lightTheme,
    })),
  changeTheme: (val) =>
    set((state) => ({
      ...state,
      theme: val,
      user: state.user
        ? {
            ...state.user,
            mode: val.palette.mode === "dark" ? UserMode.Dark : UserMode.Light,
          }
        : null,
    })),
  logout: () => set((state) => ({ ...state, user: null })),
}));
